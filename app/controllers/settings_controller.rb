class SettingsController < ApplicationController

  def language
    selected_language = params[:settings_language]
    session[:lang] = selected_language
    I18n.locale = selected_language
    redirect_to request.referer || root_path
  end

end
