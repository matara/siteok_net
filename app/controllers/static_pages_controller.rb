class StaticPagesController < ApplicationController
  def about
  end

  def contact
  end

  def home
    @top_menu = :main
    @message = Message.new(source: :home)
  end

  def customers
  end

  def internet_services
    @top_menu = :internet_services
  end

  def support_sites
    @top_menu = :support_sites
    @message = Message.new
  end

  def website_development
    @top_menu = :website_development
  end
end