class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :set_current_language

  private

  def set_current_language
    I18n.locale = @current_lang = session[:lang] || I18n.default_locale
  end

  def set_admin_locale
    I18n.locale = :en
  end

end
