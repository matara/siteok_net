class MessagesController < ApplicationController

  def create
    m = Message.new
    m.team = params[:message][:team]
    m.question = params[:message][:question]
    m.name = params[:message][:name]
    m.email = params[:message][:email]
    m.source = params[:message][:source]

    result = {}
    if m.valid?
     m.save!
      result[:status] = :done
     result[:msgs] = I18n.t("message.done_msg")
    else
      result[:status] = :error
      result[:msgs] = m.errors.full_messages
    end

    render text: result.to_json, layout: false, content_type: 'application/json; charset=utf-8'
  end


  end
