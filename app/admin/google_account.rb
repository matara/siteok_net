ActiveAdmin.register GoogleAccount do
  permit_params :account_name, :google_profile, :key_file, :account_email_address
  config.comments = true

  index do
    selectable_column
    id_column
    column :account_name
    actions
  end

  filter :account_name
  filter :google_profile
  filter :account_email_address

  show do
    attributes_table do
      row :account_name
      row :google_profile
      row :account_email_address
      row :key_file
    end
    active_admin_comments
  end

  form do |f|
    f.inputs "Project Details" do
      f.input :account_name
      f.input :google_profile
      f.input :account_email_address
      f.input :key_file, as: :file
    end
    f.actions
  end

  controller do
    def create
      params[:google_account][:key_file] = params[:google_account][:key_file].read if params[:google_account][:key_file]
      super
    end

    def update
      params[:google_account][:key_file] = params[:google_account][:key_file].read if params[:google_account][:key_file]
      super
    end
  end

end
