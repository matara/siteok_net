ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc { I18n.t("active_admin.dashboard.menu_name") }

  # Here is an example of a simple dashboard with columns and panels.
  #
  # columns do
  #   column do
  #     panel "Recent Posts" do
  #       ul do
  #         Post.recent(5).map do |post|
  #           li link_to(post.title, admin_post_path(post))
  #         end
  #       end
  #     end
  #   end

  #   column do
  #     panel "Info" do
  #       para "Welcome to ActiveAdmin."
  #     end
  #   end
  # end

  content title: proc { I18n.t("active_admin.dashboard.name") } do
    # div class: "blank_slate_container", id: "dashboard_default_message" do
    #   div class: "blank_slate" do
    #     div I18n.t("active_admin.dashboard.admins_count", count: AdminUser.count)
    #     div I18n.t("active_admin.dashboard.projects_count", count: Project.count)
    #     div I18n.t("active_admin.dashboard.ga_accounts_count", count: GaAccount.count)
    #   end
    # end

    Project.all_for_dashboard.each do |project|
      panel project.name do
        profit_partner = ProfitPartner.data(project)
        google_analytics = GoogleAnalytics.data(project)
        section do
          render partial: 'chart', locals: {
              project: project, profit_partner: profit_partner,
              google_analytics: google_analytics
          }
        end
      end
    end

    nil
  end
end
