ActiveAdmin.register Project do
  permit_params :name, :url, :profit_partner_email, :profit_partner_password, :subdomains, :live_internet_user_name,
                :google_account_id
  config.comments = true

  index do
    selectable_column
    id_column
    column :name
    column :url
    column :profit_partner_email
    actions
  end

  filter :name
  filter :url
  filter :profit_partner_email

  show do
    attributes_table do
      row :name
      row :url
      row :profit_partner_email
      row :subdomains
      row :live_internet_user_name
      row :google_account_id
    end
    active_admin_comments
  end

  form do |f|
    f.inputs "Project Details" do
      f.input :name
      f.input :url
      f.input :profit_partner_email
      f.input :profit_partner_password
      f.input :subdomains
      f.input :live_internet_user_name
      f.input :google_account_id, as: :select, collection: GoogleAccount.all { |ga| [ga.account_name, ga.id] }
    end
    f.actions
  end

  [:before_create, :before_update].each do |act|
    eval("def #{act}; p.profit_partner_password = p.profit_partner_password if p.profit_partner_password.present?; end")
  end

end
