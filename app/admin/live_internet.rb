ActiveAdmin.register_page "LiveInternet" do

  menu priority: 2, label: proc { I18n.t("active_admin.live_internet.menu_name") }

  content title: proc { I18n.t("active_admin.live_internet.name") } do

    Project.available_live_internet.each do |project|
      panel project.name do
        section do
          image_tag "http://counter.yadro.ru/logo;#{project.url}?29.1", alt: :main
        end
        if project.subdomains.present?
          section do
            project.subdomains.each do |subdomain|
              render text: (image_tag "http://counter.yadro.ru/logo;#{subdomain}.#{project.url}?29.1",
                                      title: "#{subdomain}.#{project.url}")
            end
          end
        end

        #report.each do |rep|
        #  puts rep
        #end
      end
    end

    nil
  end
end
