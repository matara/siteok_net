# -*- encoding : utf-8 -*-
require "net/http"
require "uri"
require "nokogiri"

class ProfitPartner

  LOGIN_URL = 'https://profit-partner.ru/sign/in'
  # Like 2014-05-25
  DATA_URL = "https://profit-partner.ru/stats/?site=&split_name=&interval=%{from}~%{to}&interval_from=%{from}&interval_to=%{to}&step=d&limit=100&do=statsForm-submit&all_splits=1&sort_by=0%2C%E2%96%BC"
  SAVEME = 1
  HEADERS = {'Host' => 'profit-partner.ru', 'Referer' => 'https://profit-partner.ru/'}

  class << self

    def data(project)
      return {} if project.profit_partner_email.blank? && project.profit_partner_password.blank?
      @project = project
      Rails.cache.fetch("profit_partner_project_id#{project.id}", expires_in: 1.hour) do
        data_url = DATA_URL
        data_url.sub!("%{to}", Time.current.strftime("%Y-%m-%d"))
        data_url.sub!("%{to}", Time.current.strftime("%d.%m.%Y"))
        data_url.sub!("%{from}", (Time.current - 29.days).strftime("%Y-%m-%d"))
        data_url.sub!("%{from}", (Time.current - 30.days).strftime("%Y.%m.%d"))
        puts data_url
        body = go_to_url(data_url)
        doc = Nokogiri::HTML(body)
        if doc.css('h1') && doc.css('h1').text == 'Redirect'
          spam_url = doc.css('p a').first['href']
          go_to_url(spam_url)
          body = go_to_url(data_url)
          doc = Nokogiri::HTML(body)
        end
        trs = doc.css('table.stats-table tr.d')
        res = {}
        trs.each do |tr|
          tds = tr.css('td')
          date = tds[0].text
          next if Time.current.strftime('%d.%m.%y') == date
          views = tds[2].text
          clicks = tds[3].text
          amount = tds[7].text
          res[date] = {
              views: views,
              clicks: clicks,
              amount: amount
          }
        end

        res
      end
    rescue
      {}
    end

    def go_to_url(data_url)
      uri = URI.parse(data_url)
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE

      headers = {'Cookie' => login_cookie_str(@project)}
      req = http.get2(uri, headers)
      body = req.body
      puts body
      body
    end

    def login_cookie_str(project)
      uri = URI.parse(LOGIN_URL)

      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true

      request = Net::HTTP::Post.new(uri.request_uri)
      request.set_form_data({"username" => project.profit_partner_email,
                             "password" => project.profit_partner_password,
                             "saveme" => SAVEME})
      response = http.request(request)
      response.get_fields('set-cookie').join.gsub('httponly', '')
    end
  end

end