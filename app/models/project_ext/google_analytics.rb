# -*- encoding : utf-8 -*-
require 'google/api_client'

class GoogleAnalytics

  class << self

    def data(project)
      return {} if project.google_account.blank?
      Rails.cache.fetch("google_analytics_project_id#{project.id}", expires_in: 1.hour) do
        # your GA profile id, looks like 'ga:12345'
        profile = project.google_account.google_profile

        # the path to the downloaded .p12 key file
        key_file = project.google_account.key_file

        # looks like 12345@developer.gserviceaccount.com
        service_account_email_address = project.google_account.account_email_address

        # set up a client instance
        client = Google::APIClient.new

        client.authorization = Signet::OAuth2::Client.new(
            token_credential_uri: 'https://accounts.google.com/o/oauth2/token',
            audience: 'https://accounts.google.com/o/oauth2/token',
            scope: 'https://www.googleapis.com/auth/analytics.readonly',
            issuer: service_account_email_address,
            signing_key: OpenSSL::PKCS12.new(key_file, 'notasecret').key
        ).tap { |auth| auth.fetch_access_token! }

        api_method = client.discovered_api('analytics', 'v3').data.ga.get


        # make queries
        result = client.execute(api_method: api_method, parameters: {
            ids: profile,
            'start-date' => (Date.today - 29).to_s,
            'end-date' => (Date.today - 1).to_s,
            metrics: 'ga:pageviews,ga:visitors',
            dimensions: 'ga:year,ga:month,ga:day'
        })

        res = {}
        result.data.rows.each do |row|
          date = (Time.new(row[0], row[1], row[2])).strftime("%Y-%m-%d")
          res[date] = {
              views: row[3],
              users: row[4]
          }
        end
        res
      end
    rescue
        {}
    end

  end
end