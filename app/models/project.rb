# -*- encoding : utf-8 -*-
class Project < ActiveRecord::Base
  attr_accessible :name, :url, :profit_partner_email, :profit_partner_password, :subdomains, :live_internet_user_name,
                  :google_account_id
  belongs_to :google_account

  def self.all_for_dashboard
    (available_google_account + available_profit_partner).uniq
  end

  def self.available_google_account
    where("(google_account_id IS NOT NULL OR google_account_id > '')")
  end

  def self.available_profit_partner
    where("(profit_partner_email IS NOT NULL OR profit_partner_email > '') AND (profit_partner_password IS NOT NUll AND profit_partner_password > '')")
  end

  def self.available_live_internet
    where("live_internet_user_name IS NOT NULL AND live_internet_user_name > ''")
  end

  def subdomains
    super ? super.split(',') : []
  end

  def live_internet_user_name
    return url unless super
    super
  end
end
