# -*- encoding : utf-8 -*-
class Message < ActiveRecord::Base
  validates :name, presence: true, length: {maximum: 15, minimum: 4}

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, format: {with: VALID_EMAIL_REGEX}
  #    uniqueness: { case_sensitive: false }

  validates :team, length: {minimum: 5, maximum: 25}, presence: true


  validates :question, presence: true, length: {minimum: 20, maximum: 2000}

end