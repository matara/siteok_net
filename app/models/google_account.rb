# -*- encoding : utf-8 -*-
class GoogleAccount < ActiveRecord::Base
  attr_accessible :account_name, :google_profile, :key_file, :account_email_address
  has_many :projects

  def display_name
    account_name
  end

end