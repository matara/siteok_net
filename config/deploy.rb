# config/deploy.rb
load 'deploy/assets'
require 'bundler/capistrano'
#require 'thinking_sphinx/capistrano'
#require 'sidekiq/capistrano'
#require 'whenever/capistrano'

def red(str)
  "\e[31m#{str}\e[0m"
end

# Figure out the name of the current local branch
def current_git_branch
  branch = `git symbolic-ref HEAD 2> /dev/null`.strip.gsub(/^refs\/heads\//, '')
  puts "Deploying branch #{red branch}"
  branch
end

@branch =  current_git_branch

def current_env
  case @branch
    when 'master'
      return 'production'
    when 'webmaster'
      return 'development'
    else
      return 'development'
  end
end


#set :whenever_command, 'bundle exec whenever'
set :application,     'siteok.net'
set :scm,             :git
set :repository,      'git@bitbucket.org:matara/siteok_net.git'
set :branch,          'origin/'+@branch
set :migrate_target,  :current
set :ssh_options,     { :forward_agent => true }
set :rails_env,       current_env
set :deploy_to,       '/var/www/siteok/data/apps/'+@branch+'.siteok.net/'
set :normalize_asset_timestamps, false
set :shared_children, %w(log pids sockets tmp public system db/sphinx)
set :user,            'siteok'
set :group,           'siteok'
set :use_sudo,        false

role :web,    '5.9.93.103'
role :app,    '5.9.93.103'
role :db,     '5.9.93.103', :primary => true

set(:latest_release)  { fetch(:current_path) }
set(:release_path)    { fetch(:current_path) }
set(:current_release) { fetch(:current_path) }

puts :latest_release

set(:current_revision)  { capture("cd #{current_path}; git rev-parse --short HEAD").strip }
set(:latest_revision)   { capture("cd #{current_path}; git rev-parse --short HEAD").strip }
set(:previous_revision) { capture("cd #{current_path}; git rev-parse --short HEAD@{1}").strip }

default_environment['RAILS_ENV'] = current_env

#default_environment['PATH']         = '--'
#default_environment['GEM_HOME']     = '--'
#default_environment['GEM_PATH']     = '--'
#default_environment['RUBY_VERSION'] = 'ruby-2.0.0-p0'

default_run_options[:shell] = 'bash -l'

namespace :deploy do
  desc 'Deploy your application'
  task :default do
    update
    restart
  end

  desc 'Setup your git-based deployment app'
  task :setup, :except => { :no_release => true } do
    dirs = [deploy_to, shared_path]
    dirs += shared_children.map { |d| File.join(shared_path, d) }
    run "#{try_sudo} mkdir -p #{dirs.join(' ')} && #{try_sudo} chmod g+w #{dirs.join(' ')}"
    run "git clone #{repository} #{current_path}"
  end

  task :cold do
    update
    migrate
  end

  task :update do
    transaction do
      update_code
    end
  end

  desc 'Update the deployed code.'
  task :update_code, :except => { :no_release => true } do
    run "cd #{current_path}; git fetch origin; git reset --hard #{branch}"
    finalize_update
  end

  desc 'Update the database (overwritten to avoid symlink)'
  task :migrations do
    transaction do
      update_code
    end
    migrate
    restart
  end

  task :finalize_update, :except => { :no_release => true } do
    run "chmod -R g+w #{latest_release}" if fetch(:group_writable, true)

    # mkdir -p is making sure that the directories are there for some SCM's that don't
    # save empty folders
    run <<-CMD
      rm -rf #{latest_release}/log #{latest_release}/public/system #{latest_release}/tmp/pids #{latest_release}/tmp/sockets &&
      mkdir -p #{latest_release}/public &&
      mkdir -p #{latest_release}/tmp &&
      ln -s #{shared_path}/log #{latest_release}/log &&
      ln -s #{shared_path}/system #{latest_release}/public/system &&
      ln -s #{shared_path}/pids #{latest_release}/tmp/pids &&
      ln -s #{shared_path}/sockets #{latest_release}/tmp/sockets &&
      ln -sf #{shared_path}/database.yml #{latest_release}/config/database.yml &&
      ln -sf #{shared_path}/scout.yml #{latest_release}/config/scout.yml
    CMD

    if fetch(:normalize_asset_timestamps, true)
      stamp = Time.now.utc.strftime("%Y%m%d%H%M.%S")
      asset_paths = fetch(:public_children, %w(images stylesheets javascripts)).map { |p| "#{latest_release}/public/#{p}" }.join(' ')
      run "find #{asset_paths} -exec touch -t #{stamp} {} ';'; true", :env => { 'TZ' => 'UTC' }
    end
  end

  desc 'Zero-downtime restart of Unicorn'
  task :restart, :except => { :no_release => true } do
    run "RAILS_ENV=#{rails_env} kill -s USR2 `cat #{shared_path}/pids/unicorn.pid`"
  end

  desc 'Start unicorn'
  task :start, :except => { :no_release => true } do
    run "cd #{current_path} ; RAILS_ENV=#{rails_env} bundle exec unicorn_rails -c config/unicorn.rb -D"
  end

  desc 'Stop unicorn'
  task :stop, :except => { :no_release => true } do
    run "kill -s QUIT `cat #{shared_path}/pids/unicorn.pid`"
  end

  namespace :rollback do
    desc 'Moves the repo back to the previous version of HEAD'
    task :repo, :except => { :no_release => true } do
      set :branch, 'HEAD@{1}'
      deploy.default
    end

    desc 'Rewrite reflog so HEAD@{1} will continue to point to at the next previous release.'
    task :cleanup, :except => { :no_release => true } do
      run "cd #{current_path}; git reflog delete --rewrite HEAD@{1}; git reflog delete --rewrite HEAD@{1}"
    end

    desc 'Rolls back to the previously deployed version.'
    task :default do
      rollback.repo
      rollback.cleanup
    end
  end
end

def run_rake(cmd)
  run "cd #{current_path}; #{rake} #{cmd}"
end