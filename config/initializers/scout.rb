# -*- encoding : utf-8 -*-

account_name = Rails.configuration.scout['account_name']
account_email = Rails.configuration.scout['account_email']
account_password = Rails.configuration.scout['account_password']

$scout = Scout::Account.new(account_name, account_email, account_password)