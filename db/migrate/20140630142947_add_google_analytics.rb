class AddGoogleAnalytics < ActiveRecord::Migration
  def change
    create_table :google_accounts do |t|
      t.string :account_name
      t.string :google_profile
      t.binary :key_file
      t.string :account_email_address
    end

    add_column :projects, :google_account_id, :integer
  end
end
