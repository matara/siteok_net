class AddLiveInternetToProject < ActiveRecord::Migration
  def change
    add_column :projects, :subdomains, :text
    add_column :projects, :live_internet_user_name, :string
  end
end
