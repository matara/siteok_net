class AddProfitPartnerToProject < ActiveRecord::Migration
  def change
    add_column :projects, :profit_partner_email, :string
    add_column :projects, :profit_partner_password, :string
  end
end
