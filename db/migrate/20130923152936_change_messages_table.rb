class ChangeMessagesTable < ActiveRecord::Migration
  def change
    remove_column :messages, :name
    remove_column :messages, :email
    remove_column :messages, :id_user
    add_column :messages, :user_id, :integer, default: nil, null: true
  end
end
