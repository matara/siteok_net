class FixDb < ActiveRecord::Migration
  def change
    drop_table :users
    remove_column :messages, :user_id
    add_column :messages, :name, :string
    add_column :messages, :email, :string
    add_column :messages, :source, :string
  end
end
