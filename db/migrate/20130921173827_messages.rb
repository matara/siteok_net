class Messages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :name
      t.string :email
      t.string :team
      t.text :question
      t.integer :id_user

      t.timestamps

    end
  end
end
